# TerribleTLS

This is an R6RS library for TLS (Transport Layer Security) and X.509
certificates.

It has been extracted from Industria by an extensive git filter-branch
operation.
