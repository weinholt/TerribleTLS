;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-FileCopyrightText: 2009-2022 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;;; Transport Layer Security (TLS)

;; Relevant RFCs are RFC4246 for TLS 1.1 and RFC5246 for TLS 1.2.

;; TODO: go through the implementation pitfalls in the RFC. check all
;; the MUSTs.

;; TODO: RFC 7366, encrypt-then-mac

;; TODO: RFC 7627, extended master secret

(library (terrible-tls client)
  (export
    make-tls-wrapper
    flush-tls-output
    put-tls-record get-tls-record
    put-tls-alert-record
    put-tls-handshake
    put-tls-handshake-client-hello
    put-tls-handshake-certificate
    put-tls-handshake-client-key-exchange
    put-tls-handshake-certificate-verify
    put-tls-change-cipher-spec
    put-tls-handshake-finished
    put-tls-application-data
    tls-conn-remote-certs
    tls-conn-has-unprocessed-data?)
  (import
    (except (rnrs) bytevector=?)
    (only (srfi :1 lists) last)
    (srfi :19 time)
    (hashing md5)
    (hashing sha-1)
    (hashing sha-2)
    (rename (industria bytevectors)
            (bytevector=?/constant-time bytevector=?))
    (industria crypto ec)
    (industria crypto ecdsa)
    (industria crypto dh)
    (industria crypto dsa)
    (industria crypto entropy)
    (industria crypto rsa)
    (industria buffer)
    (industria base64)
    (struct pack)
    (terrible-tls x509)
    (terrible-tls private algorithms))

;;; First the utilities

(define-syntax print
  (syntax-rules ()
    #;
    ((_ . args)
     (begin
       (for-each display (list . args))
       (newline)))
    ((_ . args) (values))))

(define (bv->string bv)
  (apply string-append
         (map (lambda (b)
                (if (< b #x10)
                    (string-append "0" (number->string b 16))
                    (number->string b 16)))
              (bytevector->u8-list bv))))

(define (bytevector-copy* bv start len)
  (let ((ret (make-bytevector len)))
    (bytevector-copy! bv start ret 0 len)
    ret))

(define (u16be-list->bytevector x)
  (uint-list->bytevector x (endianness big) 2))

(define (pad-length len blocksize)
  ;; TODO: more than the minimum padding.
  ;; Assumes block sizes are a power of two
  (if (fixnum? blocksize)
      (fxand (fx- len) (fx- blocksize 1))
      0))

;;; Version numbers

;; In TLS 1.1 the format of encrypted records changes to include an
;; explicit IV for CBC ciphers.

(define TLS-VERSION-1.3 #x0304)         ;RFC8446
(define TLS-VERSION-1.2 #x0303)         ;RFC5246
(define TLS-VERSION-1.1 #x0302)         ;RFC4346
(define TLS-VERSION-1.0 #x0301)         ;RFC2246
(define SSL-VERSION-3.0 #x0300)         ;will not be supported
(define TLS-VERSION TLS-VERSION-1.2)
(define tls-client-version-bytevector
  (pack "!S" TLS-VERSION))

;;;

(define (check-key-usage conn bitname)
  ;; Return #f if there is a key usage extension and the given bit
  ;; is not set.
  (cond ((certificate-key-usage (last (tls-conn-remote-certs conn)))
         => (lambda (ku) (memq bitname ku)))
        (else #t)))

(define (tls-conn-prf conn)
  ;; TODO: from TLS 1.2 this can also be specified by the cipher suite
  (if (< (tls-conn-version conn) TLS-VERSION-1.2)
      tls-prf-md5-sha1
      tls-prf-sha256))

(define (tls-conn-finish-hash conn)
  ;; TODO: from TLS 1.2 this can also be specified by the cipher suite
  (if (< (tls-conn-version conn) TLS-VERSION-1.2)
      (list (md5->bytevector (md5-finish (tls-conn-handshakes-md5 conn)))
            (sha-1->bytevector (sha-1-finish (tls-conn-handshakes-sha-1 conn))))
      (list (sha-256->bytevector (sha-256-finish (tls-conn-handshakes-sha-256 conn))))))

(define-record-type tls-conn
  (sealed #t)
  (fields (mutable remote-certs)      ;end-entity is last
          (mutable client-cipher)
          (mutable server-cipher)
          (mutable next-cipher)

          (mutable seq-read)
          (mutable seq-write)
          (mutable version)

          (mutable master-secret)
          (mutable client-random)
          (mutable client-write-mac-secret)
          (mutable client-write-key)
          (mutable client-write-IV)
          (mutable server-random)
          (mutable server-write-mac-secret)
          (mutable server-write-key)
          (mutable server-write-IV)
          ;; Server Diffie-Hellman parameters
          (mutable server-DH-p)
          (mutable server-DH-g)
          (mutable server-DH-Ys)
          ;; Server ECDHE parameters
          (mutable server-ECDHE-key)

          ;; Client request parameters
          (mutable client-certificate-types)
          (mutable client-certificate-authorities)

          (immutable handshakes-md5)
          (immutable handshakes-sha-1)
          (immutable handshakes-sha-256)
          (immutable server-name)
          (immutable inbuf)           ;buffer for record input
          (immutable out)             ;output port
          (immutable outb)            ;output buffer
          (immutable hsbuf)))         ;handshake buffer

;; Make a new TLS connection state.
(define (make-tls-wrapper in out server-name)
  (make-tls-conn '()
                 null-cipher-suite
                 null-cipher-suite
                 null-cipher-suite

                 0 0
                 TLS-VERSION

                 'no-master-secret-yet
                 'no-client-random
                 'no-client-write-mac-secret-yet
                 'no-client-write-key-yet
                 'no-client-write-IV-yet
                 'no-server-random
                 'no-server-write-mac-secret-yet
                 'no-server-write-key-yet
                 'no-server-write-IV-yet
                 'no-server-DH-params-yet #f #f
                 'no-server-ECDHE-key-yet

                 '() '()

                 (make-md5)
                 (make-sha-1)
                 (make-sha-256)
                 server-name
                 (make-buffer in)
                 out
                 (make-bytevector (+ (expt 2 14) 2048))
                 (make-buffer 'handshakes)))

(define (close-tls conn)
  ;; TODO: clear the connection state
  (close-port (tls-conn-out conn))
  (close-port (buffer-port (tls-conn-inbuf conn))))

;;; Record protocol

;; The record layer receives fragments (that are optionally
;; encrypted and/or compressed). These fragments are sent to a
;; higher level protocol (change-cipher-spec, alert, handshake or
;; application-data). Fragments headed for change-cipher-spec and
;; application-data can never be incomplete, so for these protocols
;; the normal tls-conn-inbuf is used. Handshakes get their own
;; buffer: tls-conn-hsbuf. The alert protocol could possibly contain
;; fragments, but is that really something that happens?

(define TLS-PROTOCOL-CHANGE-CIPHER-SPEC 20)
(define TLS-PROTOCOL-ALERT 21)
(define TLS-PROTOCOL-HANDSHAKE 22)
(define TLS-PROTOCOL-APPLICATION-DATA 23)

(define (explicit-IV-on-put? conn)
  (and (>= (tls-conn-version conn) TLS-VERSION-1.1)
       (not (eqv? 0 (cs-record-iv-size (tls-conn-client-cipher conn))))))

(define (explicit-IV-on-get? conn)
  (and (>= (tls-conn-version conn) TLS-VERSION-1.1)
       (not (eqv? 0 (cs-record-iv-size (tls-conn-server-cipher conn))))))

(define (flush-tls-output conn)
  (flush-output-port (tls-conn-out conn)))

(define (put-tls-record conn type data)
  ;; Take a bytevector, split it into records and put the records on
  ;; the output port.
  (let ((out (tls-conn-out conn))
        (len (bytevector-length data)))
    (print ";;; sending a record of type " type " and length " len)
    (cond ((> len (expt 2 14))
           (error 'put-tls-record "TODO: overlong record" len))
          ;; Pad, MAC and encrypt
          (else
           (let* ((cipher (tls-conn-client-cipher conn))
                  (extra (if (and (eqv? 4 (cs-fixed-iv-size cipher))
                                  (eqv? 8 (cs-record-iv-size cipher)))
                             16         ;the AEAD tag
                             0))
                  (blocks-len (+ len extra (cs-hash-size cipher)
                                 ;; make room for at least one
                                 ;; padding byte
                                 (if (cs-block-size cipher) 1 0)))
                  (padding (pad-length blocks-len (cs-block-size cipher)))
                  (padded-len (+ blocks-len padding))
                  (blocks (tls-conn-outb conn))
                  (IV (tls-conn-client-write-IV conn)))
             (print "#;extra " extra " hash size " (cs-hash-size cipher)
                    " unpadded len " len
                    " padded len " padded-len)
             (cond ((and (eqv? 4 (cs-fixed-iv-size cipher))
                         (eqv? 8 (cs-record-iv-size cipher)))
                    ;; In GCM there is a salt generated at the
                    ;; handshake and an explicit IV of eight bytes
                    ;; that must be unique. We use the sequence
                    ;; number.
                    (put-bytevector out (pack "!uCSS" type (tls-conn-version conn)
                                              (+ padded-len (cs-record-iv-size cipher))))
                    (set! IV (make-bytevector (+ (cs-fixed-iv-size cipher)
                                                 (cs-record-iv-size cipher))))
                    (bytevector-u32-native-set! IV 0
                                                (bytevector-u32-native-ref (tls-conn-client-write-IV conn) 0))
                    ;; This must always be unique.
                    (bytevector-u64-set! IV 4 (tls-conn-seq-write conn) (endianness big))
                    (print "#;IV " (bv->string IV))

                    ;; Send GCMNone.nonce_explicit
                    (put-bytevector out IV (cs-fixed-iv-size cipher) (cs-record-iv-size cipher)))
                   ((explicit-IV-on-put? conn)
                    ;; HMM. cs-record-iv-size
                    (put-bytevector out (pack "!uCSS" type (tls-conn-version conn)
                                              (+ padded-len (cs-record-iv-size cipher))))
                    (bytevector-randomize! (tls-conn-client-write-IV conn))
                    (put-bytevector out (tls-conn-client-write-IV conn)))
                   (else
                    (put-bytevector out (pack "!uCSS" type (tls-conn-version conn)
                                              padded-len))))
             (print "#;record-IV " (if (bytevector? (tls-conn-client-write-IV conn))
                                       (bv->string (tls-conn-client-write-IV conn))
                                       (tls-conn-client-write-IV conn)))
             (print "#;outgoing-plaintext " (bv->string data))
             (bytevector-fill! blocks 0)
             ;; Copy the plaintext.
             (bytevector-copy! data 0 blocks 0 len)
             (let ((header (pack "!uQCSS" (tls-conn-seq-write conn)
                                 type (tls-conn-version conn)
                                 len)))
               ;; TODO: if the sequence number would exceed 2^64-1, renegotiate.
               (tls-conn-seq-write-set! conn (+ (tls-conn-seq-write conn) 1))
               (when (cs-mac cipher)
                 (let ((mac (cs-compute-mac cipher (tls-conn-client-write-mac-secret conn)
                                            header data)))
                   (bytevector-copy! mac 0 blocks len (bytevector-length mac))))
               ;; After the MAC there's padding up to the block size.
               (when (cs-block-size cipher)
                 (do ((i (+ len (cs-hash-size cipher)) (+ i 1)))
                     ((= i padded-len))
                   (bytevector-u8-set! blocks i padding))) ;pad
               ;; Encrypt data + MAC + padding.
               (cs-encrypt! cipher blocks 0 blocks 0 (- padded-len extra)
                            (tls-conn-client-write-key conn)
                            IV header)
               (print "#;outgoing-ciphertext " (bv->string
                                                (subbytevector blocks 0 padded-len)))
               (put-bytevector out blocks 0 padded-len)))))))

(define (tls-conn-has-unprocessed-data? conn)
  (cond ((and (not (zero? (buffer-length (tls-conn-hsbuf conn))))
              (not (zero? (buffer-top (tls-conn-hsbuf conn)))))
         ;; If the length isn't zero, then there is unprocessed
         ;; data. If the top is zero, then there must've been an
         ;; incomplete handshake sent, and we must read more data
         ;; before anything fun can happen.
         (print ";Handshake protocol has " (buffer-length (tls-conn-hsbuf conn))
                " bytes unprocessed")
         TLS-PROTOCOL-HANDSHAKE)
        (else #f)))

(define (get-tls-record conn)
  (buffer-reset! (tls-conn-inbuf conn))
  (cond ((tls-conn-has-unprocessed-data? conn) =>
         (lambda (type)
           (print ";Handling unprocessed data of type " type)
           (handle-fragment conn (tls-conn-inbuf conn) type)))
        ((port-eof? (buffer-port (tls-conn-inbuf conn)))
         (eof-object))
        (else
         (get-tls-record* conn))))

(define (get-tls-record* conn)
  (let ((b (tls-conn-inbuf conn)))
    (buffer-reset! b)
    (buffer-read! b (format-size "!uCSS"))
    (let-values (((type version len) (unpack "!uCSS" (buffer-data b))))
      (print ";;; record type: " type
             " version: " (number->string version 16)
             " length: " len)
      (when (>= len (+ (expt 2 14) 2048))
        (error 'get-tls-record "The server sent an overlong record"
               (tls-conn-server-name conn)))
      (buffer-reset! b)
      (buffer-read! b len)
      (decipher-record conn b type len)
      (handle-fragment conn b type))))

(define (decipher-record conn b type len)
  (let ((cipher (tls-conn-server-cipher conn)))
    (define IV (tls-conn-server-write-IV conn))
    (define extra 0)
    (cond ((cs-cipher cipher)
           (print ";Deciphering " (cs-cipher cipher) " encrypted record")
           (print "#;incoming-ciphertext "
                  (bv->string
                   (bytevector-copy* (buffer-data b) (buffer-top b) len)))
           (cond
             ((and (eqv? 4 (cs-fixed-iv-size cipher))
                   (eqv? 8 (cs-record-iv-size cipher)))
              ;; GCM AEAD
              (set! IV (make-bytevector (+ (cs-fixed-iv-size cipher)
                                           (cs-record-iv-size cipher))))
              (bytevector-u32-native-set! IV 0
                                          (bytevector-u32-native-ref (tls-conn-server-write-IV conn) 0))
              (bytevector-copy! (buffer-data b) (buffer-top b)
                                IV (cs-fixed-iv-size cipher)
                                (cs-record-iv-size cipher))
              (buffer-seek! b (cs-record-iv-size cipher))
              (set! extra 16))
             ((explicit-IV-on-get? conn)
              (bytevector-copy! (buffer-data b) (buffer-top b)
                                (tls-conn-server-write-IV conn) 0
                                (cs-record-iv-size cipher))
              (buffer-seek! b (cs-record-iv-size cipher))))
           (print "#;IV " (bv->string IV))
           ;; XXX: header is for AEAD
           (let* ((len (- len extra (cs-record-iv-size cipher)))
                  (header (pack "!uQCSS"
                                (tls-conn-seq-read conn) type
                                (tls-conn-version conn)
                                ;; XXX: HMM. plaintext length + aead-length + record IV size
                                len)))
             (cs-decrypt! cipher (buffer-data b) (buffer-top b)
                          (buffer-data b) (buffer-top b)
                          len
                          (tls-conn-server-write-key conn) IV header)
             (print "#;DEC " (bv->string (bytevector-copy* (buffer-data b)
                                                           (buffer-top b)
                                                           (buffer-length b))))
             (print "#;incoming-plaintext "
                    (bv->string (bytevector-copy* (buffer-data b) (buffer-top b) len)))
             (buffer-shorten! b extra) ;remove the aead tag

             (when (cs-block-size cipher)
               ;; FIXME: verify the MAC even if the padding is crazy.
               ;; FIXME: verify the padding characters.
               (let ((padding (bytevector-u8-ref (buffer-data b) (- (buffer-bottom b) 1))))
                 (print "#;padding " padding)
                 (buffer-shorten! b (+ padding 1)))) ;remove padding
             (when (cs-mac cipher)
               (let ((mac (subbytevector (buffer-data b)
                                         (- (buffer-bottom b)
                                            (cs-hash-size cipher))
                                         (buffer-bottom b))))
                 (buffer-shorten! b (cs-hash-size cipher)) ;remove mac
                 (let* ((header (pack "!uQCSS" (tls-conn-seq-read conn)
                                      type (tls-conn-version conn)
                                      (buffer-length b)))
                        ;; TODO: MAC the data without consing
                        (data (subbytevector (buffer-data b)
                                             (buffer-top b)
                                             (buffer-bottom b)))
                        (vmac
                         (cs-compute-mac cipher (tls-conn-server-write-mac-secret conn)
                                         header data)))
                   (unless (bytevector=? mac vmac)
                     (error 'get-tls-record "bad mac" (tls-conn-server-name conn))))))

             (tls-conn-seq-read-set! conn (+ (tls-conn-seq-read conn) 1)))))))

(define (handle-fragment conn b type)
  ;; At the start of the buffer b is a plaintext fragment.
  (cond ((= type TLS-PROTOCOL-APPLICATION-DATA)
         (get-tls-application-data conn)) ;never fragmented
        ((= type TLS-PROTOCOL-CHANGE-CIPHER-SPEC)
         (get-tls-change-cipher-spec conn)) ;never fragmented
        ((= type TLS-PROTOCOL-ALERT)  ;FIXME: could be fragmented?
         (get-tls-alert-record conn))
        ((= type TLS-PROTOCOL-HANDSHAKE)
         (buffer-copy! (buffer-data b) (buffer-top b)
                       (tls-conn-hsbuf conn)
                       (buffer-length b))
         (get-tls-handshake-record conn))
        (else
         ;; TODO: send an error
         (close-tls conn)
         (error 'get-tls-record
                "The server sent an invalid record type" type))))

;;; Alert protocol

;; TODO: friendly messages for all these alerts
(define alert-descriptions
  '((0 . close-notify)
    (10 . unexpected-message)
    (20 . bad-record-mac)
    (21 . decryption-failed-RESERVED) ;never send this
    (22 . record-overflow)
    (30 . decompression-failure)
    (40 . handshake-failure)
    (41 . no-certificate-RESERVED)    ;deprecated
    (42 . bad-certificate)
    (43 . unsupported-certificate)
    (44 . certificate-revoked)
    (45 . certificate-expired)
    (46 . certificate-unknown)
    (47 . illegal-parameter)
    (48 . unknown-ca)
    (49 . access-denied)
    (50 . decode-error)
    (51 . decrypt-error)
    (60 . export-restriction-RESERVED) ;deprecated
    (70 . protocol-version)
    (71 . insufficient-security)
    (80 . internal-error)
    (90 . user-canceled)
    (100 . no-renegotiation)
    (110 . unsupported-extension)))

(define (get-tls-alert-record conn)
  (let* ((b (tls-conn-inbuf conn))
         (level (read-u8 b 0))
         (description (read-u8 b 1))
         (desc* (or (assq description alert-descriptions)
                    (cons description 'unknown-alert-description))))
    (case level
      ((2)
       (close-tls conn)
       (condition
        (make-error)
        (make-i/o-error)
        (make-i/o-port-error (buffer-port (tls-conn-inbuf conn)))
        (make-message-condition "Fatal TLS alert")
        (make-irritants-condition (list desc*))))
      ((1)
       (condition
        (make-warning)
        (make-i/o-port-error (buffer-port (tls-conn-inbuf conn)))
        (make-message-condition "TLS alert")
        (make-irritants-condition (list desc*))))
      (else
       ;; TODO: send an error
       (close-tls conn)
       (error 'get-tls-alert-record "Bad alert level sent" level)))))

(define (put-tls-alert-record conn level description)
  (assert (memv level '(1 2)))
  (put-tls-record conn TLS-PROTOCOL-HANDSHAKE
                  (pack "!CC" level description)))

;;; Handshake protocol

(define TLS-HANDSHAKE-HELLO-REQUEST 0)
(define TLS-HANDSHAKE-CLIENT-HELLO 1)
(define TLS-HANDSHAKE-SERVER-HELLO 2)
(define TLS-HANDSHAKE-CERTIFICATE 11)
(define TLS-HANDSHAKE-SERVER-KEY-EXCHANGE 12)
(define TLS-HANDSHAKE-CERTIFICATE-REQUEST 13)
(define TLS-HANDSHAKE-SERVER-HELLO-DONE 14)
(define TLS-HANDSHAKE-CERTIFICATE-VERIFY 15)
(define TLS-HANDSHAKE-CLIENT-KEY-EXCHANGE 16)
(define TLS-HANDSHAKE-FINISHED 20)

(define tls-conn-hash-handshake!
  (case-lambda
    ((conn l)
     (for-each (lambda (bv)
                 (tls-conn-hash-handshake! conn bv 0 (bytevector-length bv)))
               l))
    ((conn bv start count)
     ;; Calculate the hashes of all incoming and outgoing handshakes.
     ;; Used in the FINISH message.
     (cond
       ((< (tls-conn-version conn) TLS-VERSION-1.2)
        (md5-update! (tls-conn-handshakes-md5 conn) bv start (+ start count))
        (sha-1-update! (tls-conn-handshakes-sha-1 conn) bv start (+ start count)))
       (else
        (sha-256-update! (tls-conn-handshakes-sha-256 conn) bv start (+ start count)))))))

;; Extensions (RFC4366) are formatted in this extensive manner.
;; http://www.iana.org/assignments/tls-extensiontype-values
(define TLS-EXTENSION-SERVER-NAME 0)

(define TLS-EXTENSION-SERVER-NAME-HOSTNAME   0)
(define TLS-EXTENSION-SUPPORTED-GROUPS       10) ; RFC8422, RFC7919
(define TLS-EXTENSION-EC-POINT-FORMATS       11) ; RFC8422
(define TLS-EXTENSION-SIGNATURE-ALGORITHMS   13) ; RFC8446
(define TLS-EXTENSION-EXTENDED-MASTER-SECRET 23) ; RFC7627

(define (extensions . x)
  (bytevector-concatenate (cons (pack "!S" (bytevectors-length x)) x)))

(define (extension type data)
  (bytevector-append (pack "!SS" type (bytevector-length data)) data))

(define (ext-server-name-list names)
  (extension TLS-EXTENSION-SERVER-NAME
             (bytevector-append (pack "!S" (bytevectors-length names))
                                (bytevector-concatenate names))))

(define (extval-server-name name)
  (let ((n (string->utf8 name)))
    (bytevector-append (pack "!uCS" TLS-EXTENSION-SERVER-NAME-HOSTNAME
                             (bytevector-length n))
                       n)))

(define (ext-supported-groups groups)
  (extension TLS-EXTENSION-SUPPORTED-GROUPS
             (bytevector-append (pack "!S" (bytevectors-length groups))
                                (bytevector-concatenate groups))))

(define (ext-ec-point-formats formats)
  (extension TLS-EXTENSION-EC-POINT-FORMATS
             (bytevector-append (pack "!C" (bytevectors-length formats))
                                (bytevector-concatenate formats))))

(define (ext-signature-algorithms algos)
  (extension TLS-EXTENSION-SIGNATURE-ALGORITHMS
             (bytevector-append (pack "!S" (bytevectors-length algos))
                                (bytevector-concatenate algos))))

(define (ext-extended-master-secret)
  (extension TLS-EXTENSION-EXTENDED-MASTER-SECRET #vu8()))

(define (put-tls-handshake conn type data)
  ;; Takes data (in the form of a list of bytevectors) from a
  ;; handshake protocol and passes it to the record protocol.
  (let ((bv (make-bytevector 4))
        (len (bytevectors-length data)))
    (bytevector-u32-set! bv 0 (bitwise-ior (bitwise-arithmetic-shift-left type 24)
                                           len)
                         (endianness big))
    (tls-conn-hash-handshake! conn (cons bv data))
    (put-tls-record conn TLS-PROTOCOL-HANDSHAKE
                    (bytevector-concatenate (cons bv data)))))

(define (put-tls-handshake-client-hello conn)
  (define session-id '#vu8())
  (define cipher-suites (map cs-id (supported-cipher-suites)))
  (define compression-methods '(0))   ;null compression
  (let ((crandom (make-random-bytevector (+ 4 28))))
    ;; Client Unix time + random bytes. Common implementations seem to
    ;; use a fake time... so this does the same.
    #;
    (bytevector-u32-set! crandom 0
                         (bitwise-and #xffffffff (time-second (current-time)))
                         (endianness big))
    (tls-conn-client-random-set! conn crandom)
    (let ((hello
           (list tls-client-version-bytevector
                 crandom
                 (pack "C" (bytevector-length session-id))
                 session-id
                 (u16be-list->bytevector (cons (* 2 (length cipher-suites))
                                               cipher-suites))
                 (u8-list->bytevector (cons (length compression-methods)
                                            compression-methods))
                 (extensions
                  (ext-server-name-list
                   (list (extval-server-name (tls-conn-server-name conn))))
                  (ext-supported-groups (tls-extension-supported-groups))
                  (ext-ec-point-formats (list #vu8(0))) ;uncompressed EC points
                  ;; (ext-extended-master-secret)          ; TODO
                  ;; XXX: OpenSSL 1.1.1m responds with internal-error
                  ;; if this is not sent.
                  (ext-signature-algorithms
                   (tls-extension-supported-signature-algorithms))))))
      (put-tls-handshake conn TLS-HANDSHAKE-CLIENT-HELLO hello))))

;; `certs' are bytevectors and the last cert is the client's own
;; certificate. So the order is the same as for
;; tls-conn-remote-certs.
(define (put-tls-handshake-certificate conn certs)
  (define (put-u24 p n)
    (put-u8 p (bitwise-bit-field n 16 24))
    (put-u8 p (bitwise-bit-field n 8 16))
    (put-u8 p (bitwise-bit-field n 0 8)))
  (let ((certs (map certificate->bytevector (or certs '()))))
    (let-values (((p extract) (open-bytevector-output-port)))
      (put-u24 p (+ (bytevectors-length certs)
                    (* 3 (length certs))))
      (for-each (lambda (c)
                  (put-u24 p (bytevector-length c))
                  (put-bytevector p c))
                (reverse certs))
      (put-tls-handshake conn TLS-HANDSHAKE-CERTIFICATE
                         (list (extract))))))

(define (put-tls-handshake-client-key-exchange conn)
  (case (cs-kex (tls-conn-next-cipher conn))
    ((rsa)
     (put-tls-handshake-client-key-exchange-rsa conn))
    ((dhe-rsa dhe-dss)
     (put-tls-handshake-client-key-exchange-dhe conn))
    ((ecdhe-ecdsa)
     (put-tls-handshake-client-key-exchange-ecdhe conn))
    (else
     (error 'put-tls-handshake-client-key-exchange
            "You forgot to put in the new key exchange algorithm!"
            (cs-kex (tls-conn-next-cipher conn))))))

(define (put-tls-handshake-client-key-exchange-rsa conn)
  (print ";Client RSA Key-Exchange")
  (let ((premaster-secret (make-random-bytevector 48)))
    ;; Construct the premaster secret with our version number and 46
    ;; random bytes. Everything hinges on this being unpredictable...
    (bytevector-u16-set! premaster-secret 0 TLS-VERSION (endianness big))
    (generate-keys conn premaster-secret)
    ;; Encrypt the premaster-secret and send it to the remote.
    (unless (check-key-usage conn 'keyEncipherment)
      (close-tls conn)                ;TODO: send an error
      (error 'put-tls-handshake
             "The remote public key does not work for enciphering keys"
             (tls-conn-server-name conn)))
    (let* ((server-key (certificate-public-key
                        (last (tls-conn-remote-certs conn))))
           (keylen (rsa-public-key-byte-length server-key))
           (bv (make-bytevector (+ 2 keylen))))
      (bytevector-u16-set! bv 0 keylen (endianness big))
      (bytevector-uint-set! bv 2
                            (rsa-pkcs1-encrypt premaster-secret server-key)
                            (endianness big) keylen)
      (bytevector-fill! premaster-secret 0)
      (put-tls-handshake conn TLS-HANDSHAKE-CLIENT-KEY-EXCHANGE
                         (list bv)))))

(define (put-tls-handshake-client-key-exchange-dhe conn)
  ;; TODO: if the client certificate contains a D-H key, this
  ;; message must be empty.
  (print ";Client Diffie-Hellman Key-Exchange")
  (print "#;p-length " (bitwise-length (tls-conn-server-DH-p conn)))
  (let-values (((y Yc) (make-dh-secret (tls-conn-server-DH-g conn)
                                       (tls-conn-server-DH-p conn)
                                       (bitwise-length (tls-conn-server-DH-p conn)))))
    ;; FIXME: what is the minimum length, etc?
    (let ((Z (uint->bytevector (expt-mod (tls-conn-server-DH-Ys conn)
                                         y
                                         (tls-conn-server-DH-p conn))))
          (Yc* (uint->bytevector Yc)))
      (generate-keys conn Z)
      (bytevector-fill! Z 0)          ;XXX: not effective
      (tls-conn-server-DH-p-set! conn #f)
      (tls-conn-server-DH-g-set! conn #f)
      (tls-conn-server-DH-Ys-set! conn #f)
      ;; Send our public Diffie-Hellman value to the remote
      (print "#;Yc " (bv->string Yc*))
      (put-tls-handshake conn TLS-HANDSHAKE-CLIENT-KEY-EXCHANGE
                         (list (pack "!S" (bytevector-length Yc*))
                               Yc*)))))

(define (put-tls-handshake-client-key-exchange-ecdhe conn)
  (define who 'put-tls-handshake-client-key-exchange-ecdhe)
  ;; RFC 4492
  (print ";Client ECDHE Key-Exchange")

  (case (cs-kex (tls-conn-next-cipher conn))
    ((ecdhe-ecdsa)
     ;; ECKAS-DH1 with ECSVDP-DHC from IEEE Std 1363

     ;; "[T]he premaster secret is the x-coordinate of the ECDH shared
     ;; secret elliptic curve point represented as an octet string".
     (let* ((their-key (tls-conn-server-ECDHE-key conn))
            (curve (ecdsa-public-key-curve their-key))
            ;; That this is using ECDSA keys is not perfectly relevant
            ;; to the algorithm, it is just that that industria's
            ;; ECDSA code already has the needed helpers for what's
            ;; needed here.
            (our-key (make-ecdsa-private-key curve))
            (ecdh_Yc
             (elliptic-point->bytevector (ecdsa-public-key-Q
                                          (ecdsa-private->public our-key))
                                         curve)))
       (let* ((P (ec* (ecdsa-private-key-d our-key)
                      (ecdsa-public-key-Q their-key)
                      curve))
              (Px (car P))
              ;; FIXME: fixed length!
              (Z (uint->bytevector Px)))

         (generate-keys conn Z)
         (bytevector-fill! Z 0)          ;XXX: wishful clearing

         (put-tls-handshake conn TLS-HANDSHAKE-CLIENT-KEY-EXCHANGE
                            (list (pack "C" (bytevector-length ecdh_Yc))
                                  ecdh_Yc)))))
    (else
     (error who "implicit key exchange is not yet supported"))))

;; Generate cryptographical material from the premaster-secret
(define (generate-keys conn premaster-secret)
  (tls-conn-master-secret-set! conn ((tls-conn-prf conn) 48
                                     premaster-secret
                                     (string->utf8 "master secret")
                                     (list (tls-conn-client-random conn)
                                           (tls-conn-server-random conn))))
  (print ";plaintext premaster secret: " (bv->string premaster-secret))
  (print ";client random: " (bv->string (tls-conn-client-random conn)))
  (print ";server random: " (bv->string (tls-conn-server-random conn)))
  (print ";master secret: " (bv->string (tls-conn-master-secret conn)))
  (let* ((cipher (tls-conn-next-cipher conn))
         (hash-size (cs-hash-size cipher))
         (key-size (cs-key-length cipher))
         (IV-size (cs-fixed-iv-size cipher))
         (key-block ((tls-conn-prf conn) (* 2 (+ hash-size key-size IV-size))
                     (tls-conn-master-secret conn)
                     (string->utf8 "key expansion")
                     (list (tls-conn-server-random conn)
                           (tls-conn-client-random conn))))
         (keyp (open-bytevector-input-port key-block)))
    (tls-conn-client-write-mac-secret-set! conn (get-bytevector-n keyp hash-size))
    (tls-conn-server-write-mac-secret-set! conn (get-bytevector-n keyp hash-size))
    (let* ((client-write-key (get-bytevector-n keyp key-size))
           (server-write-key (get-bytevector-n keyp key-size)))
      (tls-conn-client-write-key-set! conn ((cs-expand-ekey cipher) client-write-key))
      (tls-conn-server-write-key-set! conn ((cs-expand-dkey cipher) server-write-key))
      (tls-conn-client-write-IV-set! conn (get-bytevector-n keyp IV-size))
      (tls-conn-server-write-IV-set! conn (get-bytevector-n keyp IV-size))
      (print ";key block: " (bv->string key-block))
      (print ";client-write-mac-secret: "
             (bv->string (tls-conn-client-write-mac-secret conn)))
      (print ";server-write-mac-secret: "
             (bv->string (tls-conn-server-write-mac-secret conn)))
      (print ";client-write-key: " (bv->string client-write-key))
      (print ";server-write-key: " (bv->string server-write-key))
      (print ";client-write-IV: " (bv->string (tls-conn-client-write-IV conn)))
      (print ";server-write-IV: " (bv->string (tls-conn-server-write-IV conn)))
      (bytevector-fill! key-block 0) ;can clear keyp
      (close-port keyp))))

(define (put-tls-handshake-certificate-verify conn private-key)
  ;; This is the message to prove that the client has the private
  ;; key for the cert it sent.

  ;; FIXME: Check that the algoritm used here is listed in the
  ;; certificate request packet.
  (cond
    ((rsa-private-key? private-key)
     (cond
       ((< (tls-conn-version conn) TLS-VERSION-1.2)
        (put-tls-handshake conn TLS-HANDSHAKE-CERTIFICATE-VERIFY
                           (let ((sig (uint->bytevector
                                       (rsa-pkcs1-encrypt-signature
                                        (bytevector-concatenate
                                         (tls-conn-finish-hash conn))
                                        private-key))))
                             (list (pack "!S" (bytevector-length sig))
                                   sig))))
       (else
        (put-tls-handshake conn TLS-HANDSHAKE-CERTIFICATE-VERIFY
                           (let ((sig (uint->bytevector
                                       (rsa-pkcs1-encrypt-digest
                                        'sha-256
                                        (bytevector-concatenate
                                         (tls-conn-finish-hash conn))
                                        private-key))))
                             (print "#;sig " (bv->string sig))

                             (list (pack "!CCS"
                                         TLS-HASH-SHA-256
                                         TLS-SIGNATURE-RSA
                                         (bytevector-length sig))
                                   sig))))))
    (else
     (assertion-violation 'put-tls-handshake-certificate-verify
                          "Unsupported client key (only RSA is supported)"))))

(define (put-tls-handshake-finished conn)
  ;; PRF(master_secret, finished_label, Hash(handshake_messages))
  ;;    [0..verify_data_length-1];
  (assert (cs-cipher (tls-conn-client-cipher conn)))
  (put-tls-handshake conn TLS-HANDSHAKE-FINISHED
                     (list ((tls-conn-prf conn)
                            (cs-verify-data-length (tls-conn-client-cipher conn))
                            (tls-conn-master-secret conn)
                            (string->utf8 "client finished")
                            (tls-conn-finish-hash conn)))))

(define (get-tls-handshake-record conn)
  (let* ((b (tls-conn-hsbuf conn))
         (type (read-u8 b 0))
         (length (read-u24 b 1))
         (start (buffer-top b)))
    (define (hash!)
      (tls-conn-hash-handshake! conn
                                (buffer-data b)
                                start
                                (+ 4 length)))
    (define (done!)
      (buffer-top-set! b (+ start 4 length))
      (when (zero? (buffer-length b)) ;end of record?
        (buffer-reset! b)))

    (buffer-seek! b 4)

    (cond ((> length (buffer-length b))
           (print ";Fragmented handshake (" (buffer-length b) " of " length ")")
           (buffer-seek! b -4)
           (get-tls-record* conn))
          ((= type TLS-HANDSHAKE-SERVER-HELLO)
           ;; The server replied to the CLIENT-HELLO message.
           ;; FIXME: check that a client-hello was sent
           (hash!)
           (print ";Server says hello.")
           (tls-conn-version-set! conn (read-u16 b 0))
           (print ";Version #x" (number->string (tls-conn-version conn) 16))
           (print ";Server time: " (read-u32 b 2))

           (tls-conn-server-random-set! conn
                                        (bytevector-copy*
                                         (buffer-data b)
                                         (+ 2 (buffer-top b))
                                         (+ 4 28)))

           (buffer-seek! b (+ 2 4 28))
           (print ";Session length: "  (read-u8 b 0))
           (buffer-seek! b (+ 1 (read-u8 b 0))) ;skip session

           (let ((id (read-u16 b 0)))
             (cond ((find (lambda (cs) (= (cs-id cs) id))
                          (supported-cipher-suites))
                    => (lambda (cs)
                         (print ";Cipher suite: " (cs-name cs))
                         (tls-conn-next-cipher-set! conn cs)))
                   (else
                    (error 'get-tls-handshake-record
                           "Bad cipher suite in server hello" conn id))))
           ;; TODO: check this
           (print ";Compression method: " (read-u8 b 2))
           (done!)
           ;; TODO: parse extensions
           'handshake-server-hello)

          ((= type TLS-HANDSHAKE-CERTIFICATE)
           (hash!)
           (print ";Server sends certificates")
           (let ((certs-end (+ (buffer-top b) 3 (read-u24 b 0))))
             (buffer-seek! b 3)
             (let lp ((certs '()))
               (cond ((= certs-end (buffer-top b))
                      (tls-conn-remote-certs-set! conn certs))
                     (else
                      (let* ((cert-len (read-u24 b 0))
                             (cert (certificate-from-bytevector (buffer-data b)
                                                                (+ 3 (buffer-top b))
                                                                (+ 3 (buffer-top b)
                                                                   cert-len))))
                        (print ";Cert of length " cert-len)
                        (print        ;cert in PEM format for debugging
                         (call-with-string-output-port
                           (lambda (p)
                             (put-delimited-base64 p "CERTIFICATE"
                                                   (bytevector-copy*
                                                    (buffer-data b)
                                                    (+ 3 (buffer-top b))
                                                    cert-len)))))
                        (buffer-seek! b (+ cert-len 3))
                        (lp (cons cert certs)))))))
           (done!)
           'handshake-certificate)

          ((= type TLS-HANDSHAKE-SERVER-KEY-EXCHANGE)
           (hash!)
           (get-tls-handshake-server-key-exchange conn b length)
           (done!)
           'handshake-server-key-exchange)

          ((= type TLS-HANDSHAKE-SERVER-HELLO-DONE)
           (hash!)
           (done!)
           'handshake-server-hello-done)

          ((= type TLS-HANDSHAKE-CERTIFICATE-REQUEST)
           (hash!)
           (get-tls-handshake-certificate-request conn b length)
           (done!)
           'handshake-certificate-request)

          ((= type TLS-HANDSHAKE-FINISHED)
           ;; If this message is correct, it proves that the server
           ;; could decrypt the pre-master secret, so it has the
           ;; private key for the certificate it sent.
           (unless (bytevector=? (bytevector-copy* (buffer-data b)
                                                   (buffer-top b)
                                                   (buffer-length b))
                                 ((tls-conn-prf conn)
                                  (cs-verify-data-length (tls-conn-client-cipher conn))
                                  (tls-conn-master-secret conn)
                                  (string->utf8 "server finished")
                                  (tls-conn-finish-hash conn)))
             (error 'get-tls-handshake "Bad verify_data in HANDSHAKE-FINISHED"))
           (hash!)
           (done!)
           (bytevector-fill! (tls-conn-master-secret conn) 0)
           (tls-conn-master-secret-set! conn 'master-secret-forgotten)
           'handshake-finished)

          ((= type TLS-HANDSHAKE-HELLO-REQUEST)
           (hash!)
           (done!)
           'hello-request)

          ;; TODO:
          (else
           (error 'get-tls-handshake-record
                  "an unknown handshake type arrived" type)))))

(define (get-tls-handshake-server-key-exchange conn b _length)
  (define who 'get-tls-handshake-server-key-exchange)
  ;; The server wants to use DHE key exchange.
  (define (get-int p)
    (bytevector->uint (get-bytevector-n p (get-unpack p "!S"))))
  (define server-key
    (certificate-public-key (last (tls-conn-remote-certs conn))))
  (print ";Server Key Exchange")
  (case (cs-kex (tls-conn-next-cipher conn))
    ((dhe-dss dhe-rsa)
     (unless (check-key-usage conn 'digitalSignature)
       (close-tls conn)                ;TODO: send an error
       (error who "The remote key cannot make signatures"
              (tls-conn-server-name conn)))
     (let ((msg (open-bytevector-input-port (buffer-data b))))
       (set-port-position! msg (buffer-top b))
       ;; Read ServerDHParams
       (let* ((p (get-int msg))
              (g (get-int msg))
              (Ys (get-int msg))
              (params
               (let ((splen (- (port-position msg) (buffer-top b))))
                 (set-port-position! msg (buffer-top b))
                 (get-bytevector-n msg splen)))
              (to-sign (list (tls-conn-client-random conn)
                             (tls-conn-server-random conn)
                             params)))
         (cond
           ((< (tls-conn-version conn) TLS-VERSION-1.2)
            ;; TLS 1.0/1.1 use the negotiated cipher suite for the
            ;; signature and hash.
            (let ((esig (get-bytevector-n msg (get-unpack msg "!S"))))
              (print "#;p #x" (bv->string (uint->bytevector p)) " #;g " g
                     " #;Ys #x" (bv->string (uint->bytevector Ys))
                     " #;esig #x" (bv->string esig))
              (case (cs-kex (tls-conn-next-cipher conn))
                ((dhe-rsa)              ;RSA
                 (let* ((digest (bytevector-append
                                 (md5->bytevector (apply md5 to-sign))
                                 (sha-1->bytevector (apply sha-1 to-sign))))
                        (esig (bytevector->uint esig))
                        (dsig (rsa-pkcs1-decrypt-signature esig server-key)))
                   (print "#;digest " (bv->string digest))
                   (print "#;dsig   " (bv->string dsig))
                   (unless (bytevector=? digest dsig)
                     (error who "bad RSA signature from the server"))))
                ((dhe-dss)              ;DSA
                 (let ((digest (sha-1->bytevector (apply sha-1 to-sign)))
                       (r/s (dsa-signature-from-bytevector esig)))
                   (print "#;digest " (bv->string digest))
                   (print "#;r/s " r/s)
                   (unless (apply dsa-verify-signature digest server-key r/s)
                     (error who "bad DSA signature from the server")))))))
           (else          ;TLS 1.2 uses a DigitallySigned struct
            (let* ((hash-algo (get-u8 msg))
                   (sig-algo (get-u8 msg))
                   (signature (get-bytevector-n msg (get-unpack msg "!S"))))
              (print  "#;p #x" (bv->string (uint->bytevector p)) " #;g " g
                      " #;Ys #x" (bv->string (uint->bytevector Ys))
                      " #;hash " hash-algo
                      " #;sig " sig-algo
                      " #;sig: #x" (bv->string signature))
              (cond
                ((eqv? sig-algo TLS-SIGNATURE-RSA)
                 (unless (rsa-public-key? server-key)
                   (error who "RSA signature with incompatible server key" server-key))
                 (let ((our-hash (tls-hash hash-algo to-sign))
                       (our-algo (tls-hash-algorithm->oid hash-algo))
                       (digest-info (rsa-pkcs1-decrypt-digest
                                     (bytevector->uint signature)
                                     server-key)))
                   (let ((sig-algo (car digest-info))
                         (sig-hash (cadr digest-info)))
                     (print "#;our-algo " our-algo)
                     (print "#;our-hash " (bv->string our-hash))
                     (print "#;sig-algo " sig-algo)
                     (print "#;sig-hash " (bv->string sig-hash))
                     (unless (and (equal? (car sig-algo) our-algo)
                                  (bytevector=? our-hash sig-hash))
                       (error who "bad RSA signature from the server")))))
                ;; XXX: DSA is missing here, but it's deprecated in TLS 1.3
                (else
                 (error who "unsupported signature algorithm" sig-algo))))))
         ;; Ok, so the owner of the key really did send these D-H
         ;; parameters. Now it's safe to use them.
         (tls-conn-server-DH-p-set! conn p)
         (tls-conn-server-DH-g-set! conn g)
         (tls-conn-server-DH-Ys-set! conn Ys))))
    ((rsa dh_dss dh_rsa) (values))

    ((ecdhe-ecdsa)                      ; RFC 4492
     (let ((msg (open-bytevector-input-port (buffer-data b))))
       (set-port-position! msg (buffer-top b))
       (let ((curve-type (get-u8 msg)))
         (define explicit_prime 1)
         (define explicit_char2 2)
         (define named_curve 3)
         (unless (eqv? curve-type named_curve)
           (error who "Unsupported curve type" curve-type))
         (let* ((named-curve (get-unpack msg "!S"))
                (public-bv (get-bytevector-n msg (get-unpack msg "C")))
                ;; This public key is ephemeral and is signed with the
                ;; server key.
                (public-key
                 (cond ((= named-curve TLS-GROUP-SECP256R1)
                        (make-ecdsa-public-key secp256r1 public-bv))
                       ((= named-curve TLS-GROUP-SECP384R1)
                        (make-ecdsa-public-key secp384r1 public-bv))
                       ((= named-curve TLS-GROUP-SECP521R1)
                        (make-ecdsa-public-key secp521r1 public-bv))
                       (else
                        (error who "Unsupported elliptic curve" named-curve))))
                (params
                 (let ((splen (- (port-position msg) (buffer-top b))))
                   (set-port-position! msg (buffer-top b))
                   (get-bytevector-n msg splen)))
                (to-sign (list (tls-conn-client-random conn)
                               (tls-conn-server-random conn)
                               params))
                (hash-algo (get-u8 msg))
                (sig-algo (get-u8 msg))
                (signature (get-bytevector-n msg (get-unpack msg "!S"))))
           (unless (eqv? sig-algo TLS-SIGNATURE-ECDSA)
             (error who "unsupported ecdhe-ecdsa signature algorithm" sig-algo))
           (let ((r+s (ecdsa-signature-from-bytevector signature))
                 (our-hash (tls-hash hash-algo to-sign)))
             (unless (eqv? #t (ecdsa-verify-signature our-hash server-key
                                                      (car r+s) (cadr r+s)))
               (error who "bad ECDSA signature from the server"))
             (tls-conn-server-ECDHE-key-set! conn public-key))))))

    (else
     (error who "You forgot to put in the new key exchange algorithm!"
            (cs-kex (tls-conn-next-cipher conn))))))

(define (get-tls-handshake-certificate-request conn b length)
  (define (get-opaque p len-format)
    (let* ((len (get-unpack p len-format))
           (bv (get-bytevector-n p len)))
      (unless (and (bytevector? bv)
                   (= (bytevector-length bv) len))
        (error 'get-tls-handshake-certificate-request
               "bad certificate request" bv len))
      bv))
  (let ((p (open-bytevector-input-port
            (bytevector-copy* (buffer-data b) (buffer-top b) length))))
    ;; TODO: read the CAs.
    (print ";Client certificate request: "
           (bytevector-copy* (buffer-data b) (buffer-top b) length))
    (let ((certificate-types (bytevector->u8-list (get-opaque p "C"))))
      (tls-conn-client-certificate-types-set! conn certificate-types))))

;;; Change cipher spec protocol

;; TODO: RFC 5746

(define (put-tls-change-cipher-spec conn)
  (put-tls-record conn TLS-PROTOCOL-CHANGE-CIPHER-SPEC #vu8(1))
  (tls-conn-seq-write-set! conn 0)
  (tls-conn-client-cipher-set! conn (tls-conn-next-cipher conn)))

(define (get-tls-change-cipher-spec conn)
  (cond ((and (= (buffer-length (tls-conn-inbuf conn)) 1)
              (= (read-u8 (tls-conn-inbuf conn) 0) 1)
              (not (cs-cipher (tls-conn-server-cipher conn)))) ;a precaution
         (print ";Switching to encrypted records")
         (tls-conn-server-cipher-set! conn (tls-conn-next-cipher conn))
         (tls-conn-seq-read-set! conn 0)
         'change-cipher-spec)
        (else
         ;; TODO
         (close-tls conn)
         (error 'get-tls-change-cipher-spec
                "The server sent a bad change-cipher-spec message"))))

;;; Application data protocol

(define (put-tls-application-data conn data)
  (assert (cs-cipher (tls-conn-client-cipher conn)))
  (put-tls-record conn TLS-PROTOCOL-APPLICATION-DATA data))

(define (get-tls-application-data conn)
  (assert (cs-cipher (tls-conn-server-cipher conn)))
  (let* ((b (tls-conn-inbuf conn))
         ;; TODO: return buffer, top and length. no need to copy.
         (appdata (bytevector-copy* (buffer-data b)
                                    (buffer-top b)
                                    (buffer-length b))))
    (print "#;application-data " appdata)

    (list 'application-data appdata))))
